# shoppingcart

> Generaic Basket Shopping Cart

## Build Setup

Shopping Cart

To view the icons in component we required following file
bootstrap.min.css

Place following element to your html where you want to visible this component
<div id="app-myshop-shoppingcart"></div>

### Shopping cart as a Menu Item
`app-myshop-shoppingcart-parent` 's `data-show-as-menu` will be used to control this behaviour.
Set `data-show-as-menu=1` to enable `data-show-as-menu=0` to disable.

### Showing cart items on menu item hover
Add the class `app-myshop-shoppingcart-onhover` to the menu item to enable this behaviour.
Important: This will work only if the `data-show-as-menu` is set to `1`.

### Showing cart items count on menu item
Add the following element to show the cart item count on the menu item.
```
<span class="app-myshop-shoppingcart-count"></<span>
```

## How to apply the theme SAAS

Add following structure to your scss file and apply your theme variables as value
```
.myshp-shpcart-container {
	.myshp-basket-count{

	}

	.myshp-toggle-cart {
		.myshp-img{

		}
	  	.myshp-icon{

	  	}
	}

	.myshp-toggle-cart-arrow {

	}

	.myshp-cart-content-container {
	  	.myshp-empty{
			.myshp-title{

			}
			.myshp-content{

			}
		}
		.myshp-order-lines{
			.myshp-basket-product{
				.myshp-product-image{

				}
				.myshp-product-id{

				}
				.myshp-product-title{

				}
				.myshp-product-description{

				}
				.myshp-product-quantity{

				}
				.myshp-product-unitprice{

				}
				.myshp-product-linetotal{

				}
				.myshp-product-action-controls{
					.myshp-btn-change{

					}
					.myshp-btn-change-more-info{

					}
					.myshp-btn-remove{

					}
				}
			}

			.myshp-order-line-feature{
				.myshp-shipping-label{

				}
				.myshp-shipping-value{

				}

				.myshp-action-label{

				}
				.myshp-action-value{

				}
			}

			.myshp-cart-total{
				.myshp-total-label{

				}
				.myshp-total-value{

				}
			}

			.myshp-control-buttons{
				.left{
					.myshp-make-quotation{

					}
				}
				.right{
					.myshp-direct-order{

					}
				}
			}
		}
	}
}
```