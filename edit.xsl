<?xml version="1.0"?>
<!--
    @(#)$HeadURL: https://svn1.sliksvn.com/design/editor_client/myshop/5/default/BlockManager/Blocks/ShoppingCart/edit.xsl $ $Date: 2018-05-09 08:25:27 +0200 (Wed, 09 May 2018) $

	Copyright 1999-2018(c) MijnWinkel B.V. Rijnegomlaan 33, Aerdenhout,
	North Holland, NL-2111XM, The Netherlands All rights reserved.

    This software is the confidential and proprietary information of MijnWinkel
    B.V. ("Confidential Information"). You shall not disclose such Confidential
    Information and shall use it only in accordance with the terms of the license
    agreement you entered into with MijnWinkel.

    Edit page for the shopping cart block.

    @version    $Revision: 29132 $ $Author: john $
    @since      7-may-2018
    @author     John Zanoni

-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    exclude-result-prefixes="xsl">

    <xsl:import href="file:///usr/local/web/system/resources/com/myshop/5/default/Library/base.xsl"/>
    <xsl:import
        href="file:///usr/local/web/system/resources/com/myshop/5/default/BlockManager/include_block_manager.xsl"/>


    <xsl:variable name="BlockType">ShoppingCart</xsl:variable>

    <!-- Needed parameters -->
    <xsl:template name="InsertParameters">

        <!-- mark as vue block -->
        <input type="hidden" name="block:vue" value="1"/>

    </xsl:template>

    <xsl:template name="GetBlockHead">

        <script type="text/javascript" xml:space="preserve">
           
            function dialogBindings(me){

                $(".js-onclick-resource-clear").on("click",function(){
                    doCommand('update');
                });
                
            }

            function resourceSelected(input){
                $("[name='block:width'], [name='validate__block:width'], [name='block:origwidth']").val(input.width);
                $("[name='block:height'], [name='validate__block:height'], [name='block:origheight']").val(input.height);
               doCommand('update');
            }
            
            function dialogPreviewContent(){
                var url=$("[name='block:image']").val();
                
                if(""==url)url="/pic/leeg.gif";
                else if(0!=url.indexOf("https"))url="/styles/myshop-app/dist/images/nossl_image_"+("<xsl:value-of select="$ActiveEditorLanguage"/>".toLowerCase())+".jpg";
                
                return '&lt;img class="img-responsive" src="'+url+'"&gt;';
            }

            function calculatesize(newwidth, newheight) {
                var origwidth = $("[name='block:origwidth']").val();
                var origheight = $("[name='block:origheight']").val();
                var ratio = origwidth/origheight;
                
                if (newwidth != "") {
                    newwidth = parseFloat(newwidth);
                    newheight = Math.round(newwidth/ratio);
                    
                } else if (newheight != "") {
                    newheight = parseFloat(newheight);
                    newwidth = Math.round(ratio * newheight);
                    
                }

                if(isNaN(newwidth)) newwidth = 0;
                if(isNaN(newheight)) newheight = 0;

                $("[name='block:width'], [name='validate__block:width']").val(newwidth);
                $("[name='block:height'], [name='validate__block:height']").val(newheight);
            }
            
            function dialogCommandHandler(cmd, nav){
            
                if($("[name='switch_block:proportional']").is(":checked")) {
                    var thisElement=nav.get("this");
                    if(thisElement){
                        var name=thisElement.attr('name');
                        
                        if("block:width"==name)calculatesize($("[name='validate__block:width']").val(), "");
                        else if("block:height"==name)calculatesize("", $("[name='validate__block:height']").val());
                    }
                }
                return true;
            }
           
        </script>

    </xsl:template>

    <xsl:template name="GetBlockContent">

        <div class="row">
            <div class="myshop-bo-section-summary">
                <h1>
                    <myshop_resource id="icon_title"><![CDATA[Icon]]></myshop_resource>
                </h1>

                <p>
                    <myshop_resource id="icon_subtitle"><![CDATA[]]></myshop_resource>
                </p>
            </div>

            <div class="myshop-bo-section-content">


                <div class="form-group">

                    <xsl:call-template name="InsertLabelDefault">
                        <xsl:with-param name="LabelText">
                            <myshop_resource id="picture"><![CDATA[URL icon]]></myshop_resource>
                        </xsl:with-param>
                    </xsl:call-template>

                    <div class="row">

                        <xsl:call-template name="InsertResourceSelector">
                            <xsl:with-param name="Name" select="'block:image'"/>
                            <xsl:with-param name="Id" select="'myshop-bo-block-logo'"/>
                            <xsl:with-param name="Label" select="''"/>
                            <xsl:with-param name="Class" select="'rectangle'"/>
                            <xsl:with-param name="Value" select="key('block-key', 'block:image')"/>
                            <xsl:with-param name="Preview" select="true()"/>
                            <xsl:with-param name="Size" select="'6'"/>
                            <xsl:with-param name="SizeLg" select="'6'"/>
                            <xsl:with-param name="Location" select="'absolute'"/>
                            <xsl:with-param name="FilterGroup" select="'image'"/>
                        </xsl:call-template>

                    </div>

                </div>


                <div class="form-group">

                    <xsl:call-template name="InsertLabelDefault">
                        <xsl:with-param name="LabelText">
                            <myshop_resource id="proportion"
                                ><![CDATA[Preserve width and height proportion:]]></myshop_resource>
                        </xsl:with-param>
                    </xsl:call-template>

                    <xsl:call-template name="InsertSwitchElement">
                        <xsl:with-param name="Name" select="'block:proportional'"/>
                        <xsl:with-param name="Value" select="'1'"/>
                        <xsl:with-param name="Checked"
                            select="boolean(key('block-key', 'block:proportional') = 1)"/>
                        <xsl:with-param name="InfoText">
                            <myshop_resource id="proportional_info"/>
                        </xsl:with-param>
                    </xsl:call-template>

                </div>

                <div class="form-group">

                    <xsl:call-template name="InsertLabelDefault">
                        <xsl:with-param name="LabelText">
                            <myshop_resource id="block_width"><![CDATA[Width]]></myshop_resource>
                        </xsl:with-param>
                    </xsl:call-template>

                    <xsl:call-template name="InsertInputElement">
                        <xsl:with-param name="Name" select="'block:width'"/>
                        <xsl:with-param name="Class" select="'number'"/>
                        <xsl:with-param name="Value" select="key('block-key', 'block:width')"/>
                        <xsl:with-param name="Size" select="'3'"/>
                        <xsl:with-param name="Placeholder">
                            <myshop_resource id="width_placeholder"/>
                        </xsl:with-param>
                        <xsl:with-param name="InfoText">
                            <myshop_resource id="width_info"/>
                        </xsl:with-param>
                    </xsl:call-template>
                    <input type="hidden" name="block:origwidth">
                        <xsl:attribute name="value">
                            <xsl:value-of select="key('block-key', 'block:origwidth')"/>
                        </xsl:attribute>
                    </input>
                </div>

                <div class="form-group">

                    <xsl:call-template name="InsertLabelDefault">
                        <xsl:with-param name="LabelText">
                            <myshop_resource id="block_height"><![CDATA[Height]]></myshop_resource>
                        </xsl:with-param>
                    </xsl:call-template>

                    <xsl:call-template name="InsertInputElement">
                        <xsl:with-param name="Name" select="'block:height'"/>
                        <xsl:with-param name="Class" select="'number'"/>
                        <xsl:with-param name="Value" select="key('block-key', 'block:height')"/>
                        <xsl:with-param name="Size" select="'3'"/>
                        <xsl:with-param name="Placeholder">
                            <myshop_resource id="height_placeholder"/>
                        </xsl:with-param>
                        <xsl:with-param name="InfoText">
                            <myshop_resource id="height_info"/>
                        </xsl:with-param>
                    </xsl:call-template>

                    <input type="hidden" name="block:origheight">
                        <xsl:attribute name="value">
                            <xsl:value-of select="key('block-key', 'block:origheight')"/>
                        </xsl:attribute>
                    </input>

                </div>

            </div>
        </div>

        <xsl:call-template name="InsertLine"/>

        <div class="row">
            <div class="myshop-bo-section-summary">
                <h1>
                    <myshop_resource id="settings_title"
                        ><![CDATA[Column settings]]></myshop_resource>
                </h1>

                <p>
                    <myshop_resource id="settings_subtitle"><![CDATA[]]></myshop_resource>
                </p>
            </div>

            <div class="myshop-bo-section-content">

                <div class="form-group">
                    <xsl:call-template name="InsertLabelDefault">
                        <xsl:with-param name="LabelText">
                            <myshop_resource id="show_id_label"
                                ><![CDATA[Show the column with the product id]]></myshop_resource>
                        </xsl:with-param>
                    </xsl:call-template>

                    <xsl:call-template name="InsertSwitchElement">
                        <xsl:with-param name="Name" select="'block:show-id'"/>
                        <xsl:with-param name="Value" select="1"/>
                        <xsl:with-param name="ValueOff" select="0"/>
                        <xsl:with-param name="Class" select="'myshop-bo-control'"/>
                        <xsl:with-param name="Checked"
                            select="not('0' = $BlockData/properties/property[@name = 'block:show-id'])"/>
                        <xsl:with-param name="InfoText">
                            <myshop_resource id="show_id_info"/>
                        </xsl:with-param>
                    </xsl:call-template>
                </div>

                <div class="form-group">
                    <xsl:call-template name="InsertLabelDefault">
                        <xsl:with-param name="LabelText">
                            <myshop_resource id="show_description_label"
                                ><![CDATA[Show the column with the description]]></myshop_resource>
                        </xsl:with-param>
                    </xsl:call-template>

                    <xsl:call-template name="InsertSwitchElement">
                        <xsl:with-param name="Name" select="'block:show-description'"/>
                        <xsl:with-param name="Value" select="1"/>
                        <xsl:with-param name="ValueOff" select="0"/>
                        <xsl:with-param name="Class" select="'myshop-bo-control'"/>
                        <xsl:with-param name="Checked"
                            select="not('0' = $BlockData/properties/property[@name = 'block:show-description'])"/>
                        <xsl:with-param name="InfoText">
                            <myshop_resource id="show_description_info"/>
                        </xsl:with-param>
                    </xsl:call-template>
                </div>
                <div class="form-group">
                    <xsl:call-template name="InsertLabelDefault">
                        <xsl:with-param name="LabelText">
                            <myshop_resource id="show_qunatity_label"
                                ><![CDATA[Show the column with the qunatity]]></myshop_resource>
                        </xsl:with-param>
                    </xsl:call-template>

                    <xsl:call-template name="InsertSwitchElement">
                        <xsl:with-param name="Name" select="'block:show-qunatity'"/>
                        <xsl:with-param name="Value" select="1"/>
                        <xsl:with-param name="ValueOff" select="0"/>
                        <xsl:with-param name="Class" select="'myshop-bo-control'"/>
                        <xsl:with-param name="Checked"
                            select="not('0' = $BlockData/properties/property[@name = 'block:show-quantity'])"/>
                        <xsl:with-param name="InfoText">
                            <myshop_resource id="show_qunatity_info"/>
                        </xsl:with-param>
                    </xsl:call-template>
                </div>

                <div class="form-group">
                    <xsl:call-template name="InsertLabelDefault">
                        <xsl:with-param name="LabelText">
                            <myshop_resource id="show_units_label"
                                ><![CDATA[Show the column with the units]]></myshop_resource>
                        </xsl:with-param>
                    </xsl:call-template>

                    <xsl:call-template name="InsertSwitchElement">
                        <xsl:with-param name="Name" select="'block:show-units'"/>
                        <xsl:with-param name="Value" select="1"/>
                        <xsl:with-param name="ValueOff" select="0"/>
                        <xsl:with-param name="Class" select="'myshop-bo-control'"/>
                        <xsl:with-param name="Checked"
                            select="'1' = $BlockData/properties/property[@name = 'block:show-units']"/>
                        <xsl:with-param name="InfoText">
                            <myshop_resource id="show_units_info"/>
                        </xsl:with-param>
                    </xsl:call-template>
                </div>

                <div class="form-group">
                    <xsl:call-template name="InsertLabelDefault">
                        <xsl:with-param name="LabelText">
                            <myshop_resource id="show_price_label"
                                ><![CDATA[Show the column with the product price]]></myshop_resource>
                        </xsl:with-param>
                    </xsl:call-template>

                    <xsl:call-template name="InsertSwitchElement">
                        <xsl:with-param name="Name" select="'block:show-price'"/>
                        <xsl:with-param name="Value" select="1"/>
                        <xsl:with-param name="ValueOff" select="0"/>
                        <xsl:with-param name="Class" select="'myshop-bo-control'"/>
                        <xsl:with-param name="Checked"
                            select="not('0' = $BlockData/properties/property[@name = 'block:show-price'])"/>
                        <xsl:with-param name="InfoText">
                            <myshop_resource id="show_price_info"/>
                        </xsl:with-param>
                    </xsl:call-template>
                </div>

                <div class="form-group">
                    <xsl:call-template name="InsertLabelDefault">
                        <xsl:with-param name="LabelText">
                            <myshop_resource id="show_line_total_label"
                                ><![CDATA[Show the column with the sub total]]></myshop_resource>
                        </xsl:with-param>
                    </xsl:call-template>

                    <xsl:call-template name="InsertSwitchElement">
                        <xsl:with-param name="Name" select="'block:show-line_total'"/>
                        <xsl:with-param name="Value" select="1"/>
                        <xsl:with-param name="ValueOff" select="0"/>
                        <xsl:with-param name="Class" select="'myshop-bo-control'"/>
                        <xsl:with-param name="Checked"
                            select="not('0' = $BlockData/properties/property[@name = 'block:show-line_total'])"/>
                        <xsl:with-param name="InfoText">
                            <myshop_resource id="show_line_total_info"/>
                        </xsl:with-param>
                    </xsl:call-template>
                </div>


            </div>


        </div>
        <xsl:call-template name="InsertLine"/>

        <div class="row">
            <div class="myshop-bo-section-summary">
                <h1>
                    <myshop_resource id="actions_title"><![CDATA[Actions]]></myshop_resource>
                </h1>

                <p>
                    <myshop_resource id="action_subtitle"><![CDATA[]]></myshop_resource>
                </p>
            </div>

            <div class="myshop-bo-section-content">
                <div class="form-group">
                    <xsl:call-template name="InsertLabelDefault">
                        <xsl:with-param name="LabelText">
                            <myshop_resource id="icon_action_label"
                                ><![CDATA[Click on the icon action]]></myshop_resource>
                        </xsl:with-param>
                    </xsl:call-template>

                    <xsl:variable name="OnClick">
                        <xsl:choose>
                            <xsl:when
                                test="'' = normalize-space($BlockData/properties/property[@name = 'block:onclick'])"
                                >0</xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of
                                    select="$BlockData/properties/property[@name = 'block:onclick']"
                                />
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <div class="margin-bottom-10">

                        <div class="row">

                            <div class="col-sm-4">

                                <xsl:call-template name="InsertInputRadioElement">
                                    <xsl:with-param name="Name" select="'block:onclick'"/>
                                    <xsl:with-param name="Value" select="'0'"/>
                                    <xsl:with-param name="Checked" select="'0' = $OnClick"/>
                                    <xsl:with-param name="Class"
                                        select="'js-onchange-update myshop-bo-control'"/>
                                    <xsl:with-param name="Label">
                                        <myshop_resource id="icon_action_none_label"
                                            ><![CDATA[Do nothing (default)]]></myshop_resource>
                                    </xsl:with-param>
                                </xsl:call-template>

                            </div>

                            <div class="col-sm-7">

                                <div class="margin-bottom-5">
                                    <!-- no selection option -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="margin-bottom-10">

                        <div class="row">

                            <div class="col-sm-4">

                                <xsl:call-template name="InsertInputRadioElement">
                                    <xsl:with-param name="Name" select="'block:onclick'"/>
                                    <xsl:with-param name="Value" select="'1'"/>
                                    <xsl:with-param name="Checked" select="'1' = $OnClick"/>
                                    <xsl:with-param name="Class"
                                        select="'js-onchange-update myshop-bo-control'"/>
                                    <xsl:with-param name="Label">
                                        <myshop_resource id="icon_action_basket_label"
                                            ><![CDATA[Go to the shopping cart]]></myshop_resource>
                                    </xsl:with-param>
                                </xsl:call-template>

                            </div>

                            <div class="col-sm-7">

                                <div class="margin-bottom-5">
                                    <!-- no selection option -->
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="margin-bottom-10">

                        <div class="row">

                            <div class="col-sm-4">

                                <xsl:call-template name="InsertInputRadioElement">
                                    <xsl:with-param name="Name" select="'block:onclick'"/>
                                    <xsl:with-param name="Value" select="'2'"/>
                                    <xsl:with-param name="Checked" select="'2' = $OnClick"/>
                                    <xsl:with-param name="Class"
                                        select="'js-onchange-update myshop-bo-control'"/>
                                    <xsl:with-param name="Label">
                                        <myshop_resource id="icon_action_address_label"
                                            ><![CDATA[Go to the addess page (one-page-checkout)]]></myshop_resource>
                                    </xsl:with-param>
                                </xsl:call-template>

                            </div>

                            <div class="col-sm-7">

                                <div class="margin-bottom-5">
                                    <!-- no selection option -->
                                </div>
                            </div>
                        </div>
                    </div>

                </div>



                <div class="form-group">
                    <xsl:call-template name="InsertLabelDefault">
                        <xsl:with-param name="LabelText">
                            <myshop_resource id="show_action_to_checkout_label"
                                ><![CDATA[Show the to checkout button]]></myshop_resource>
                        </xsl:with-param>
                    </xsl:call-template>

                    <xsl:call-template name="InsertSwitchElement">
                        <xsl:with-param name="Name" select="'block:show-action-to-checkout'"/>
                        <xsl:with-param name="Value" select="1"/>
                        <xsl:with-param name="ValueOff" select="0"/>
                        <xsl:with-param name="Class" select="'myshop-bo-control'"/>
                        <xsl:with-param name="Checked"
                            select="not('0' = $BlockData/properties/property[@name = 'block:show-action-to-checkout'])"/>
                        <xsl:with-param name="InfoText">
                            <myshop_resource id="show_action_to_checkout_info"/>
                        </xsl:with-param>
                    </xsl:call-template>
                </div>

                <div class="form-group">
                    <xsl:call-template name="InsertLabelDefault">
                        <xsl:with-param name="LabelText">
                            <myshop_resource id="show_action_quotation_label"
                                ><![CDATA[Show the quotation button]]></myshop_resource>
                        </xsl:with-param>
                    </xsl:call-template>

                    <xsl:call-template name="InsertSwitchElement">
                        <xsl:with-param name="Name" select="'block:show-action-quotation'"/>
                        <xsl:with-param name="Value" select="1"/>
                        <xsl:with-param name="ValueOff" select="0"/>
                        <xsl:with-param name="Class" select="'myshop-bo-control'"/>
                        <xsl:with-param name="Checked"
                            select="'1' = $BlockData/properties/property[@name = 'block:show-action-quotation']"/>
                        <xsl:with-param name="InfoText">
                            <myshop_resource id="show_action_quotation_info"/>
                        </xsl:with-param>
                    </xsl:call-template>
                </div>

                <div class="form-group">
                    <xsl:call-template name="InsertLabelDefault">
                        <xsl:with-param name="LabelText">
                            <myshop_resource id="show_action_change_moreinfo_label"
                                ><![CDATA[Show the change remove line action]]></myshop_resource>
                        </xsl:with-param>
                    </xsl:call-template>

                    <xsl:call-template name="InsertSwitchElement">
                        <xsl:with-param name="Name" select="'block:show-action-remove'"/>
                        <xsl:with-param name="Value" select="1"/>
                        <xsl:with-param name="ValueOff" select="0"/>
                        <xsl:with-param name="Class" select="'myshop-bo-control'"/>
                        <xsl:with-param name="Checked"
                            select="not('0' = $BlockData/properties/property[@name = 'block:show-action-remove'])"/>
                        <xsl:with-param name="InfoText">
                            <myshop_resource id="show_action_remove_info"/>
                        </xsl:with-param>
                    </xsl:call-template>
                </div>
                <div class="form-group">
                    <xsl:call-template name="InsertLabelDefault">
                        <xsl:with-param name="LabelText">
                            <myshop_resource id="show_action_change_label"
                                ><![CDATA[Show the change quantity options]]></myshop_resource>
                        </xsl:with-param>
                    </xsl:call-template>
                    <xsl:call-template name="InsertSwitchElement">
                        <xsl:with-param name="Name" select="'block:show-action-change'"/>
                        <xsl:with-param name="Value" select="1"/>
                        <xsl:with-param name="ValueOff" select="0"/>
                        <xsl:with-param name="Class" select="'myshop-bo-control'"/>
                        <xsl:with-param name="Checked"
                            select="not('0' = $BlockData/properties/property[@name = 'block:show-action-change'])"/>
                        <xsl:with-param name="InfoText">
                            <myshop_resource id="show_action_change_info"/>
                        </xsl:with-param>
                    </xsl:call-template>
                </div>
                <div class="form-group">
                    <xsl:call-template name="InsertLabelDefault">
                        <xsl:with-param name="LabelText">
                            <myshop_resource id="show_action_change_moreinfo_label"
                                ><![CDATA[Show the change quantity on the more info page option]]></myshop_resource>
                        </xsl:with-param>
                    </xsl:call-template>

                    <xsl:call-template name="InsertSwitchElement">
                        <xsl:with-param name="Name" select="'block:show-action-change-moreinfo'"/>
                        <xsl:with-param name="Value" select="1"/>
                        <xsl:with-param name="ValueOff" select="0"/>
                        <xsl:with-param name="Class" select="'myshop-bo-control'"/>
                        <xsl:with-param name="Checked"
                            select="'1' = $BlockData/properties/property[@name = 'block:show-action-change-moreinfo']"/>
                        <xsl:with-param name="InfoText">
                            <myshop_resource id="show_action_change_moreinfo_info"/>
                        </xsl:with-param>
                    </xsl:call-template>
                </div>



            </div>

        </div>
        <xsl:call-template name="InsertLine"/>

        <div class="row">
            <div class="myshop-bo-section-summary">
                <h1>
                    <myshop_resource id="integration_title"
                        ><![CDATA[Integration]]></myshop_resource>
                </h1>

                <p>
                    <myshop_resource id="integration_subtitle"><![CDATA[
                        To integrate the block insert a DIV element with the id <b>app-myshop-shoppingcart</b> in the skin or template of the page. 
                        <br/>
                        <br/>
                        <b>&lt;div id="app-myshop-shoppingcart"&gt;<span style="color:green">&lt;!-- shopping cart --&gt;</span>&lt;/div&gt;</b>
                        
                        ]]></myshop_resource>
                </p>
            </div>

            <div class="myshop-bo-section-content">

                <div class="form-group">
                    <!-- Show open menu items -->

                    <xsl:call-template name="InsertLabelDefault">
                        <xsl:with-param name="LabelText">
                            <myshop_resource id="integration_label"
                                ><![CDATA[Block is part of the template/skin]]></myshop_resource>
                        </xsl:with-param>
                    </xsl:call-template>

                    <xsl:call-template name="InsertSwitchElement">
                        <xsl:with-param name="Name" select="'block:default_integration'"/>
                        <xsl:with-param name="Value" select="1"/>
                        <xsl:with-param name="ValueOff" select="0"/>
                        <xsl:with-param name="Class" select="'myshop-bo-control'"/>
                        <xsl:with-param name="Checked"
                            select="'1' = $BlockData/properties/property[@name = 'block:default_integration']"/>
                        <xsl:with-param name="InfoText">
                            <myshop_resource id="integration_info"/>
                        </xsl:with-param>
                    </xsl:call-template>
                </div>

            </div>
        </div>

    </xsl:template>

</xsl:stylesheet>
