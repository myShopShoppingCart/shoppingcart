<?xml version="1.0"?>
<!--
    @(#)$HeadURL: https://svn1.sliksvn.com/design/editor_client/myshop/5/default/BlockManager/Blocks/ShoppingCart/generate.xsl $ $Date: 2018-05-28 08:37:26 +0200 (Mon, 28 May 2018) $
    
	Copyright 1999-2018(c) MijnWinkel B.V. Rijnegomlaan 33, Aerdenhout,
	North Holland, NL-2111XM, The Netherlands All rights reserved.
    
    This software is the confidential and proprietary information of MijnWinkel
    B.V. ("Confidential Information"). You shall not disclose such Confidential
    Information and shall use it only in accordance with the terms of the license
    agreement you entered into with MijnWinkel.
    
    Generate page for the shopping cart block.
    
    @version    $Revision: 29157 $ $Author: john $
    @since      7-may-2018
    @author     John Zanoni

-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    exclude-result-prefixes="xsl">

    <xsl:import
        href="file:///usr/local/web/system/resources/com/myshop/default/Components/components_generate.xsl"/>

    <xsl:import
        href="file:///usr/local/web/system/resources/com/myshop/5/default/BlockManager/Blocks/TopBakkersOrderList/script.xsl"/>

    <xsl:output indent="no" method="html"/>

    <xsl:variable name="BlockType">MyshopConfigurator</xsl:variable>
    <xsl:variable name="DefaultClass">
        <xsl:choose>
            <xsl:when
                test="not(key('block-key', 'block:default_class')) or key('block-key', 'block:default_class') = '1'"
                >myshp_block product_configurator_box</xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="CustomClass">
        <xsl:choose>
            <xsl:when
                test="not(key('block-key', 'block:custom_class')) or key('block-key', 'block:custom_class') = ''"/>
            <xsl:otherwise>
                <xsl:value-of select="key('block-key', 'block:custom_class')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="ClassName">
        <xsl:value-of select="$DefaultClass"/>
        <xsl:value-of select="$CustomClass"/>
    </xsl:variable>

    <xsl:variable name="ShowId">
        <xsl:choose>
            <xsl:when test="'' = normalize-space(key('block-key', 'block:show-id'))">1</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="key('block-key', 'block:show-id')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <xsl:variable name="ShowDescription">
        <xsl:choose>
            <xsl:when test="'' = normalize-space(key('block-key', 'block:show-description'))"
                >1</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="key('block-key', 'block:show-description')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <xsl:variable name="ShowQuantity">
        <xsl:choose>
            <xsl:when test="'' = normalize-space(key('block-key', 'block:show-quantity'))"
                >1</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="key('block-key', 'block:show-quantity')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>


    <xsl:variable name="ShowPrice">
        <xsl:choose>
            <xsl:when test="'' = normalize-space(key('block-key', 'block:show-price'))">1</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="key('block-key', 'block:show-price')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <xsl:variable name="ShowSubTotalLine">
        <xsl:choose>
            <xsl:when test="'' = normalize-space(key('block-key', 'block:show-line-total'))"
                >1</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="key('block-key', 'block:show-line-total')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <xsl:variable name="ShowActionChange">
        <xsl:choose>
            <xsl:when test="'' = normalize-space(key('block-key', 'block:show-action-change'))"
                >1</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="key('block-key', 'block:show-action-change')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>

    <xsl:variable name="ShowActionRemove">
        <xsl:choose>
            <xsl:when test="'' = normalize-space(key('block-key', 'block:show-action-remove'))"
                >1</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="key('block-key', 'block:show-action-remove')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="OnClick">
        <xsl:choose>
            <xsl:when test="'' = normalize-space(key('block-key', 'block:onclick'))"
                >0</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="key('block-key', 'block:onclick')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="ShowQuotation">
        <xsl:choose>
            <xsl:when test="'' = normalize-space(key('block-key', 'block:show-action-quotation'))"
                >0</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="key('block-key', 'block:show-action-quotation')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <xsl:variable name="ShowToCheckout">
        <xsl:choose>
            <xsl:when test="'' = normalize-space(key('block-key', 'block:show-action-to-checkout'))"
                >1</xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="key('block-key', 'block:show-action-to-checkout')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    
    <!-- Page entry point -->
    <xsl:template match="/">
        <div id="app-myshop-shoppingcart-parent" style="display:none" data-show-id="{$ShowId}"
            data-icon-width="{key('block-key', 'block:width')}"
            data-icon-height="{key('block-key', 'block:height')}"
            data-onclick="{$OnClick}"
            data-show-to-checkout="{$ShowToCheckout}"
            data-show-make-quotation="{$ShowQuotation}"
            data-show-description="{$ShowDescription}" data-show-quantity="{$ShowQuantity}"
            data-show-units="{key('block-key', 'block:show-units')}" data-show-price="{$ShowPrice}"
            data-show-line-total="{$ShowSubTotalLine}" data-show-action-change="{$ShowActionChange}"
            data-show-action-change-moreinfo="{key('block-key', 'block:show-action-change-moreinfo')}"
            data-show-action-remove="{$ShowActionRemove}">
            <xsl:attribute name="data-icon"><xsl:value-of select="key('block-key', 'block:image')" disable-output-escaping="yes"/></xsl:attribute>
            <xsl:comment>component</xsl:comment>
            
        </div>

        <xsl:choose>
            <xsl:when test="'1' = key('block-key', 'block:default_integration')">
                <!-- is part of the more info tempate -->
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="$ClassName != ''">
                        <div class="{$ClassName}">
                            <xsl:call-template name="InsertBlockNameAsTitle">
                                <xsl:with-param name="ShowName"
                                    select="key('block-key', 'block:show_name')"/>
                                <xsl:with-param name="BlockName"
                                    select="key('block-key', 'block:name')"/>
                            </xsl:call-template>
                            <xsl:choose>
                                <xsl:when test="key('block-key', 'block:same_row') = '1'">
                                    <xsl:call-template name="InsertLoader"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <!-- <div class="myshp_box"> -->
                                    <xsl:call-template name="InsertLoader"/>
                                    <!-- </div> -->
                                </xsl:otherwise>
                            </xsl:choose>
                        </div>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="InsertBlockNameAsTitle">
                            <xsl:with-param name="ShowName"
                                select="key('block-key', 'block:show_name')"/>
                            <xsl:with-param name="BlockName" select="key('block-key', 'block:name')"
                            />
                        </xsl:call-template>
                        <xsl:choose>
                            <xsl:when test="key('block-key', 'block:same_row') = '1'">
                                <xsl:call-template name="InsertLoader"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <!-- <div class="myshp_box"> -->
                                <xsl:call-template name="InsertLoader"/>
                                <!-- </div> -->
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
        
     
    </xsl:template>


    <xsl:template name="InsertLoader">
        <div id="app-myshop-shoppingcart">
            <xsl:comment>component</xsl:comment>
        </div>
    </xsl:template>
</xsl:stylesheet>
