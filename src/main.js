/* eslint-disable */

/*
 * Copyright 1999-2023(c) MijnWinkel B.V. Noothoven van Goorstraat 11E, Gouda,
 * South Holland, NL-2806RA, The Netherlands All rights reserved.
 * 
 * This software is the confidential and proprietary information of MijnWinkel
 * B.V. ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the license
 * agreement you entered into with MijnWinkel.
 * 
 */
import Vue from 'vue'
import App from './App'
import { MyShopClient } from 'myshop'
import { EventBus } from './event-bus';

Vue.config.productionTip = false
window.Vue = Vue

const PACKAGE = "Shoppingcart";
const VERSION = "1.1.2";
const TEST_MODE = false;

//console.log(PACKAGE, VERSION);


document.log=function(msg){
  if(Vue.config.devtools||TEST_MODE){
  if(arguments){
      msg="";
      for(let i=0;i<arguments.length;i++){
        if((typeof arguments[i] === 'function') || (typeof arguments[i] === 'object')){
          console.log(msg);
          console.log(arguments[i]);
          msg="";
        }
        else{
          msg+=arguments[i];
          msg+=" ";
        }
      }
    }
    console.log(msg);
  }
}

/* eslint-disable no-new */
setTimeout(function () {

  myshopEvent().on('order',function(){

    EventBus.$emit('update');

    //$('.shpcart-action-reload-order-list').trigger('click');

    let cont = MyShopClient.getDataAttribute("app-myshop-shoppingcart-parent", 'continue');
    if("0"===cont){
      myshopEvent().proceed(false);
      return false;
    }
    else return myshopEvent().proceed(true);
  });

  new Vue({
    el: '#app-myshop-shoppingcart',
    components: { App },
    template: '<App/>'
  });



}, 200);
